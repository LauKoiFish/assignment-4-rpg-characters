# RPG Characters

## Description

This is a project created in Noroff Accelerate .NET Full Stack -course. <br />
The goal was to create a console application where you are able to create an RPG character and items such as armor and weapons.<br />
Characters can equip items and level up, and stats will automatically update accordingly.<br />
We have four character classes to choose from: Mage, Ranger, Rouge, and Warrior.<br />

## Getting Started
Clone the project and modify it how you please to make it fit your own project.<br />
Simply clone the repository and open it with Visual Studio.


## Usage

Unfortunately this is still work in progress so most of the functionality is missing.

## Authors

Contributors for this project:
[Laura Koivuranta](https://gitlab.com/LauKoiFish)
