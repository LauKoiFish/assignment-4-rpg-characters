﻿namespace Assignment_4_RPG_Characters;

public class ExceptionMessages
{
    public class InvalidItemLevelException : Exception
    {
        public override string Message => "Required level not yet acheved. Try again in the future.";
    }
    public class InvalidWeaponException : Exception
    {
        public override string Message => "This is not a proper weapon for you. Choose something else.";
    }
    public class InvalidArmorException : Exception
    {
        public override string Message => "This is not a proper armor for you. Choose something else.";
    }
}
