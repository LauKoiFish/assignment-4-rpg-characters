﻿using Assignment_4___RPG_Characters.CharacterClasses;
using static Assignment_4_RPG_Characters.Items.BaseItemEnums;

namespace Assignment_4_RPG_Characters.CharacterClasses;

public class BaseMageClass : BaseCharacterClass
{
    public BaseMageClass()
    {
        Type = CharacterType.Mage;
        Strength = 1;
        Dexterity = 1;
        Intelligence = 8;
        StrengthIncreacement = 1;
        DexterityIncreacement = 1;
        IntelligenceIncreacement = 5;
        AllowedWeaponTypes.AddRange(new List<WeaponTypes>()
        {
            WeaponTypes.Staff,
            WeaponTypes.Wand
        });
        AllowedArmorTypes.Add(ArmorTypes.Cloth);

    }

}