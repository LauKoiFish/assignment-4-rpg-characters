﻿using Assignment_4___RPG_Characters.CharacterClasses;
using static Assignment_4___RPG_Characters.CharacterClasses.CharacterClassTypes;
using static Assignment_4_RPG_Characters.Items.BaseItemEnums;

namespace Assignment_4_RPG_Characters.CharacterClasses;

public class BaseCharacterClass
{
    public string CharacterClassName { get; set; } = "";
    public int CharacterLevel { get; set; } = 1;
    public CharacterType Type { get; init; }

    // Item types
    public WeaponTypes WeaponType { get; set; }
    public ArmorTypes ArmorType { get; set; }
    public ItemSlots ItemSlot { get; set; }

    // Stat increacement when a character levels up
    public int StrengthIncreacement { get; set; }
    public int DexterityIncreacement { get; set; }
    public int IntelligenceIncreacement { get; set; }

    // Allowed types of items
    public List<WeaponTypes> AllowedWeaponTypes { get; set; } = new List<WeaponTypes>();
    public List<ArmorTypes> AllowedArmorTypes { get; set; } = new List<ArmorTypes>();

    public class WeaponAttributes
    {
        public double BaseDmg { get; set; }

        public double AttacksPerSecond { get; set; }

        public WeaponAttributes()
        {

        }
    }

}
