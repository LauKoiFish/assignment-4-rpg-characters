﻿using Assignment_4___RPG_Characters.CharacterClasses;
using static Assignment_4_RPG_Characters.Items.BaseItemEnums;

namespace Assignment_4_RPG_Characters.CharacterClasses;

public class BaseRangerClass : BaseCharacterClass
{
    public BaseRangerClass()
    {
        Type = CharacterType.Ranger;
        Strength = 1;
        Dexterity = 7;
        Intelligence = 1; 
        StrengthIncreacement = 1;
        DexterityIncreacement = 5;
        IntelligenceIncreacement = 1;
        AllowedArmorTypes.AddRange(new List<ArmorTypes>()
        {
            ArmorTypes.Leather,
            ArmorTypes.Mail
        });
        AllowedWeaponTypes.Add(WeaponTypes.Bow);

    }
}
