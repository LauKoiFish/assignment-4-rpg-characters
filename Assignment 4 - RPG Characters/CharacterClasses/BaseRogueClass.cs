﻿using Assignment_4___RPG_Characters.CharacterClasses;
using static Assignment_4_RPG_Characters.Items.BaseItemEnums;

namespace Assignment_4_RPG_Characters.CharacterClasses;

public class BaseRogueClass : BaseCharacterClass
{
    public BaseRogueClass()
    {
        Type = CharacterType.Rouge;
        Strength = 2;
        Dexterity = 6;
        Intelligence = 1;
        StrengthIncreacement = 1;
        DexterityIncreacement = 4;
        IntelligenceIncreacement = 1;
        AllowedArmorTypes.AddRange(new List<ArmorTypes>()
        {
            ArmorTypes.Leather,
            ArmorTypes.Mail
        });
        AllowedWeaponTypes.AddRange(new List<WeaponTypes>()
        {
            WeaponTypes.Dagger,
            WeaponTypes.Sword
        });

    }
}
