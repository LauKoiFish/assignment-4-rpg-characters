﻿using Assignment_4___RPG_Characters.CharacterClasses;
using static Assignment_4_RPG_Characters.Items.BaseItemEnums;

namespace Assignment_4_RPG_Characters.CharacterClasses;

public class BaseWarriorClass : BaseCharacterClass
{
 
    public BaseWarriorClass()
    {
        Type = CharacterType.Warrior;
        Strength = 5;
        Dexterity = 2;
        Intelligence = 1;
        StrengthIncreacement = 3;
        DexterityIncreacement = 2;
        IntelligenceIncreacement = 1;
        AllowedArmorTypes.AddRange(new List<ArmorTypes>()
        {
            ArmorTypes.Mail,
            ArmorTypes.Plate
        });
        AllowedWeaponTypes.AddRange(new List<WeaponTypes>()
        {
            WeaponTypes.Axe,
            WeaponTypes.Hammer,
            WeaponTypes.Sword
        });

    }
}
