﻿using Assignment_4___RPG_Characters.CharacterClasses;
using Assignment_4_RPG_Characters.Items;

namespace Assignment_4___RPG_Characters.Items
{
    public class PrimaryAttribute
    {
        public double Strength { get; set; }
        public double Dexterity { get; set; }
        public double Intelligence { get; set; }

        public PrimaryAttribute(double strength, double dexterity, double intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        public PrimaryAttribute()
        {

        }

        // Defines which class will be leveled up and then increases each stat according to class specification.
        public PrimaryAttribute LevelUp(CharacterClassTypes.CharacterType characterType)
        {
            switch (characterType)
            {
                case CharacterClassTypes.CharacterType.Mage:
                    Strength += 1;
                    Dexterity += 1;
                    Intelligence += 5;
                    return this;
                case CharacterClassTypes.CharacterType.Ranger:
                    Strength += 1;
                    Dexterity += 5;
                    Intelligence += 1;
                    return this;
                case CharacterClassTypes.CharacterType.Rouge:
                    Strength += 1;
                    Dexterity += 4;
                    Intelligence += 1;
                    return this;
                case CharacterClassTypes.CharacterType.Warrior:
                    Strength += 3;
                    Dexterity += 2;
                    Intelligence += 1;
                    return this;
                default:
                    break;
            }

            return this;
        }

        // Takes in an instance of this class and adds the content to the current instance.
        public PrimaryAttribute Add(PrimaryAttribute attributes)
        {
            Strength += attributes.Strength;
            Dexterity += attributes.Dexterity;
            Intelligence += attributes.Intelligence;

            return this;
        }
    }

}
