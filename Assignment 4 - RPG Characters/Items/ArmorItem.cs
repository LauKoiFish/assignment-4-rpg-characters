﻿using Assignment_4___RPG_Characters.Items;

namespace Assignment_4_RPG_Characters.Items
{
    public class Armor : BaseItem
    {
        public PrimaryAttribute BaseAttributes { get; set; }
        public BaseItemEnums.ArmorTypes ArmorType { get; set; }
        public BaseItemEnums.ItemSlots ItemSlot { get; set; }

        public Armor(string name, int requiredLvl, BaseItemEnums.ArmorTypes type, BaseItemEnums.ItemSlots slot) : base(name, requiredLvl)
        {
            ArmorType = type;
            ItemSlot = slot;

            if (requiredLvl > 0 && requiredLvl <= 3)
            {
                BaseAttributes = new PrimaryAttribute();
            }
            else if (requiredLvl > 3 && requiredLvl <= 6)
            {
                BaseAttributes = new PrimaryAttribute();
            }
            else if (requiredLvl > 6 && requiredLvl <= 9)
            {
                BaseAttributes = new PrimaryAttribute();
            }
            else
            {
                BaseAttributes = new PrimaryAttribute(0, 0, 0);
            }
        }
    }

}
