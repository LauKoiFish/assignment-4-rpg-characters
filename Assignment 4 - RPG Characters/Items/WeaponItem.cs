﻿using Assignment_4_RPG_Characters.Items;
using static Assignment_4_RPG_Characters.CharacterClasses.BaseCharacterClass;

namespace Assignment_4_RPG_Characters.Items
{
    public class Weapon : BaseItem
    {
        public WeaponAttributes Attributes { get; set; }
        public BaseItemEnums.WeaponTypes WeaponType { get; set; }
        public BaseItemEnums.ItemSlots ItemSlot { get; set; }

        // Calculating DPS = Damage * Attack speed.
        public double DamagePerSecond { get => Attributes.BaseDmg * Attributes.AttacksPerSecond; }
        
        // Determing base damage and attack speed according to level.
        public Weapon(string name, int requiredLvl, BaseItemEnums.WeaponTypes weaponType) : base(name, requiredLvl)
        {
            WeaponType = weaponType;
            ItemSlot = BaseItemEnums.ItemSlots.Weapon;

            if (requiredLvl > 0 && requiredLvl <= 3)
            {
                Attributes = new WeaponAttributes()
                {
                    BaseDmg = 2,
                    AttacksPerSecond = 2,
                };
            }
            else if (requiredLvl > 3 && requiredLvl <= 6)
            {
                Attributes = new WeaponAttributes()
                {
                    BaseDmg = 5,
                    AttacksPerSecond = 5
                };
            }
            else if (requiredLvl > 6 && requiredLvl <= 9)
            {
                Attributes = new WeaponAttributes()
                {
                    BaseDmg = 10,
                    AttacksPerSecond = 10
                };
            }
            else
            {
                Attributes = new WeaponAttributes();
            }

        }
    }

}
