﻿using System.Xml.Linq;

namespace Assignment_4_RPG_Characters.Items;

public class BaseItem
{
    // Properties
    public string ItemName { get; set; } = "";
    public int RequiredLevel { get; set; }
    public string SlotToEquip { get; set; } = "";

    public BaseItem(string name, int requiredLvl)
    {
        ItemName = name;
        RequiredLevel = requiredLvl;

    }


}
