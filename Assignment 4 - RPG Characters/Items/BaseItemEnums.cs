﻿namespace Assignment_4_RPG_Characters.Items;

public class BaseItemEnums : BaseItem
{
    public BaseItemEnums(string name, int reqLvl) : base(name, reqLvl)
    {
    }

    // Here are the different weapon types.
    public enum WeaponTypes
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }

    // Here are the different armor types.
    public enum ArmorTypes
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }

    // Different slots where one can equip armors & weapons.
    public enum ItemSlots
    {
        Head,
        Body,
        Legs,
        Weapon
    }
}
